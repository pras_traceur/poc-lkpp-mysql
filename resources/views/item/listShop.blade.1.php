@extends('layouts.app')

@section('content')




<div class="container">
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fas fa-times"></span></a>
        <p>{{ $message }}</p>
    </div>
@endif

    <br/>
    <h1 class="text-center">Daftar Barang</h1>
    <br/>    
    <table class="table table-striped table-hover" id="items-table" style="background-color: white;">
        <thead>
            <tr>
                <th style="max-width: 20px;">No</th>
                <th>Nama</th>
                <th>Deskripsi</th>
                <th>Harga</th>
                <th>Stok</th>
                <th style="max-width: 100px;">Action</th>
            </tr>
        </thead>
    </table>

                    <div class="modal fade" id="modal_shop" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Beli Barang</h4>
                            </div>

                            <form class="form-horizontal" method="POST" action="{{ route('beli.Item') }}">
                            {{ csrf_field() }}
                        
                            <div class="modal-body" style="margin-bottom: 20px;">
            
                                <div class="form-group">
                                    <label class="control-label col-xs-4" >Nama Barang</label>
                                    <div class="col-xs-8" style="margin-top: 5px;">
                                        <span id="nama"></span>
                                    </div>
                                </div>                                        
            
                                <div class="form-group">
                                    <label class="control-label col-xs-4" >Harga</label>
                                    <div class="col-xs-8" style="margin-top: 5px;">
                                        <span id="harga"></span>
                                    </div>                                    
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-xs-4" >Stok</label>
                                    <div class="col-xs-8" style="margin-top: 5px;">
                                        <span id="stok"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-xs-4" >Jumlah Pembelian</label>
                                    <div class="col-xs-8" style="margin-top: 5px;">
                                        <input type="number" id="jumlah" name="jumlah" step="1" min="1" width="20px" style="width: 50px; padding-left: 3px;">
                                    </div>
                                </div>

                                <input type="hidden" name="id_user" id="id_user" value="{{ Auth::user()->id }}">
                                <input type="hidden" name="id_items" id="id_items">
            
                            </div>
                            <div style=" margin-left: 30px;"><span>Tekan <b>Submit</b> untuk melanjutkan pembelian</span></div>
            
                            <div class="modal-footer">
                                <input class="btn btn-primary" type="submit" value="Submit" onclick="return valid_assign()"/>
                            </div>

                            </form>
                            
                        </div>
                        </div>
                    </div>

    <script>
    $(function() {

        var table = $('#items-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('getItems.datas') !!}',
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'nama', name: 'nama' , searchable: true},
                { data: 'deskripsi', name: 'deskripsi' , searchable: true},
                { data: null, name: 'harga', searchable: true, render: function ( data, type, row ) {
                    return data.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                } },
                { data: 'stok', name: 'stok' , searchable: true},                
                { data: null, name: 'action', render: function ( data, type, row ) {
                    return '<button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal_shop" onclick="pilih(\'' + data.id + '\', \'' + data.id_user + '\', \'' + data.nama + '\', \'' + data.harga + '\', \'' + data.stok + '\')"><i class="fa fa-shopping-cart"></i> Beli</button>';
                } },
            ],
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }]
        });
        
    });

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus data? \n\nJabatan: " + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    function valid_assign() {
        var res = true;


        return res;
    }

    function pilih(id_items, id_user, nama, harga, stok) {
        $("#nama").html(nama);
        $("#harga").html(harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $("#stok").html(stok);
        $("#jumlah").val(1);
        $("#jumlah").attr('max', stok);
        $("#id_items").val(id_items);
        //$("#id_user").val(id_user);
    }

    </script>
    @stack('scripts')

</div>
@endsection

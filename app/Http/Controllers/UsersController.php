<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Jabatan;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Exception;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        //return DataTables::of(User::query())->make(true);
        // $users = User::select('*')->get();
        // return Datatables::of($users)->make(true);
    }

    public function addUser(Request $request)
    {
        $data = array(
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'address' => $request->address,
            'hp' => $request->phone,
            'npwp' => $request->npwp,
            'role_id' => $request->role,
            'jabatan_id' => $request->jabatan
        );

        try {
            DB::table('users')->insert($data);
        } catch(Exception $e) {
            return redirect()->route('home')
                        ->with('fail','Gagal membuat user baru!');
        }
        
        return redirect()->route('home')
                        ->with('success','User created successfully');
        
    }

    public function addUserPage() 
    {        
        $roles = Role::all();
        $jabatans = Jabatan::all();

        return View::make('addUser', compact('roles','jabatans'));
    }

    public function editUserPage($id) 
    {
        $user = User::find($id);
        $roles = Role::all();
        $jabatans = Jabatan::all();
        return View::make('editUser', compact('user','roles','jabatans'));
    }

    public function viewUserPage($id) 
    {
        $user = User::find($id);
        $roles = Role::all();
        $jabatans = Jabatan::all();
        return View::make('viewUser', compact('user','roles','jabatans'));
    }

    public function editUser(Request $request)
    {
        $data = array(
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'hp' => $request->phone,
            'npwp' => $request->npwp,
            'jabatan_id' => $request->jabatan,
            'role_id' => $request->role
        );

        if(!empty($request->password))
            $data['password'] = bcrypt($request->password);

        DB::table('users')->where('id','=',$request->id)->update($data);

        return redirect()->route('home')
                        ->with('success','User edited successfully');
    }

    public function deleteUser($id)
    {
        User::find($id)->delete();
        return redirect()->route('home')
                        ->with('success','User deleted successfully');
    }

    public function profile()
    {
        return view('profile');
    }

    public function isAdmin()
    {
        $users = DB::table ( 'users' )
        ->select ( 'users.id',
            'users.name',
            'users.email',
            'users.created_at',
            'users.updated_at'       
        );
        //return DataTables::of(User::query())->make(true);
        // $users = User::select('*')->get();
        // return Datatables::of($users)->make(true);
    }

    public function getDatas(Request $request){
        //echo"masuk method getDatas";    

        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'name',
                2 =>'email',
                3 =>'address',
                4 =>'hp',
                5 =>'role',
                6 =>'jabatan',
        );
        //Getting the data
        $users = DB::table ( 'users' )
        ->join('roles', 'users.role_id', '=', 'roles.id')
        ->leftjoin('jabatans', 'users.jabatan_id', '=', 'jabatans.id')
        ->select ( 'users.id',
            'users.name',
            'users.email',
            'users.role_id',
            'users.address',
            'users.hp',
            'roles.name as role',
            'jabatans.name as jabatan'
        );
        // $users = DB::table ( 'users' )
        // ->where('users.group_id','=',$group_id)
        // ->select ( 'users.id',
        //     'users.user_name',
        //     'users.email',
        //     'users.dob',
        // );
        $totalData = $users->count ();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
            }
        }

        $i = 0;

        if ($request->has ( 'columns.1.search' )) {
            if ($request->input ( 'columns.1.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.1.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'columns.2.search' )) {
            if ($request->input ( 'columns.2.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.2.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'users.email', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'columns.3.search' )) {
            if ($request->input ( 'columns.3.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.3.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'users.address', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'columns.4.search' )) {
            if ($request->input ( 'columns.4.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.4.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'users.hp', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'columns.5.search' )) {
            if ($request->input ( 'columns.5.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.5.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'jabatans.name', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'columns.6.search' )) {
            if ($request->input ( 'columns.6.search.value' ) != '') {
                $searchTerm = $request->input ( 'columns.6.search.value' );
                /*
                * Seach clause : we only allow to search on user_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $users->where ( 'roles.name', 'Like', '%' . $searchTerm . '%' );
            }
        }
        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                //$jobs->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
                $users->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $users->count ();
        // Data to client
        $jobs = $users->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $users = $users->get ();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1;
        $role = Auth::user()->role_id;

        foreach ( $users as $user ) {
            $nestedData = array ();
            //$nestedData [0] = $user->id;
            $nestedData ['no'] = ++$start;
            $nestedData ['name'] = $user->name;
            $nestedData ['email'] = $user->email;
            $nestedData ['address'] = $user->address;
            $nestedData ['hp'] = $user->hp;
            $nestedData ['role'] = $user->role;
            $nestedData ['jabatan'] = $user->jabatan;
            $actionBtn = '<div style="text-align: center"><a href="viewUserPage/'.$user->id.'" class="btn btn-xs btn-primary"><i class="fa fa-search"></i> View</a> ';
            if($role == 1) {
                $actionBtn .= '<a href="editUserPage/'.$user->id.'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a> 
                    <a href="#delete-'.$user->id.'" class="btn btn-xs btn-primary" onclick=\'confirmDel('.$user->id.',"'.$user->name.'")\'><i class="fa fa-trash"></i> Delete</a>
                    <form class="delete" action="delUser/'.$user->id.'" method="get">
                    <input type="hidden" class="form-control" name="id" value="'.$user->id.'">                    
                    <input id="delButton'.$user->id.'" type="submit" value="Delete" style="display: none;">
                    </form></div>';            
            }
            $nestedData ['action'] = $actionBtn;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        //echo"$tableContent: $tableContent";
        return $tableContent;
    }

    public function getBladeExcel()
    {
        $userData = DB::table ( 'users' )
        ->join('roles', 'users.role_id', '=', 'roles.id')
        ->leftjoin('jabatans', 'users.jabatan_id', '=', 'jabatans.id')
        ->select ( 'users.id',
            'users.name',
            'users.email',
            'users.address',
            'users.hp',
            'users.npwp',
            'roles.name as role',
            'jabatans.name as jabatan'
        )->get ();

        \Excel::create('Data Karyawan', function($excel) use($userData) {

            $excel->sheet('Data Karyawan', function($sheet) use($userData) {

                $excelData = [];
                $excelData[] = [
                    'No',
                    'Name',
                    'E-mail',
                    'Address',
                    'Phone',
                    'NPWP',
                    'Jabatan',
                    'Role',
                ];

                $no = 1;
                foreach ($userData as $key => $value) {
                    $excelData[] = [
                        $no++,
                        $value->name,
                        $value->email,
                        $value->address,
                        $value->hp,
                        $value->npwp,
                        $value->jabatan,
                        $value->role
                    ];                    
                }

                $sheet->fromArray($excelData, null, 'A1', true, false);

            });

        })->download('xlsx');

    }

    public function getJson()
    {
        $userData = User::all();

        //return Response::json($userData);

        $data = json_encode($userData);
        $file = time() . '_file.json';
        $destinationPath=public_path()."/upload/json/";
        if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
        File::put($destinationPath.$file,$data);
        return response()->download($destinationPath.$file);

    }
    
}

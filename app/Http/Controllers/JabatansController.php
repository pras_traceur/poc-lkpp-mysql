<?php

namespace App\Http\Controllers;

use App\Jabatan;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JabatansController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function addJabatan(Request $request)
    {
        $data = array(
            'name' => $request->name
        );

        DB::table('jabatans')->insert($data);

        return redirect()->route('listJabatan')
                        ->with('success','Jabatan created successfully');
    }

    public function addJabatanPage() 
    {        
        return View::make('addJabatan');
    }

    public function editJabatanPage($id) 
    {
        $jabatans = Jabatan::find($id);
        return View::make('jabatan.editJabatan', compact('jabatans'));
    }

    public function viewJabatanPage($id) 
    {
        
    }

    public function editJabatan(Request $request)
    {
        $data = array(
            'name' => $request->name
        );

        DB::table('jabatans')->where('id','=',$request->id)->update($data);

        return redirect()->route('listJabatan')
                        ->with('success','Jabatan edited successfully');
    }

    public function deleteJabatan($id)
    {
        Jabatan::find($id)->delete();
        return redirect()->route('listJabatan')
                        ->with('success','Jabatan deleted successfully');
    }

    public function getDatas(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'name',
        );
        //Getting the data
        $jabatans = DB::table ( 'jabatans' )
        ->select ( 'id',
            'name'
        );
        
        $totalData = $jabatans->count ();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on jabatan_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $jabatans->where ( 'name', 'Like', '%' . $searchTerm . '%' );
            }
        }
        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $jabatans->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $jabatans->count ();
        // Data to client
        $jobs = $jabatans->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $jabatans = $jabatans->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1;
        foreach ( $jabatans as $jabatan ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['name'] = $jabatan->name;            
            
            $actionBtn = '<a href="editJabatanPage/'.$jabatan->id.'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a> 
                <a href="#delete-'.$jabatan->id.'" class="btn btn-xs btn-primary" onclick=\'confirmDel('.$jabatan->id.',"'.$jabatan->name.'")\'><i class="fa fa-trash"></i> Delete</a>
                <form class="delete" action="delJabatan/'.$jabatan->id.'" method="get">
                <input type="hidden" class="form-control" name="id" value="'.$jabatan->id.'">
                <input id="delButton'.$jabatan->id.'" type="submit" value="Delete" style="display: none;">
                </form></div>';                        
            $nestedData ['action'] = $actionBtn;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }
    
}

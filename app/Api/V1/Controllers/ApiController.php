<?php

namespace App\Api\V1\Controllers;

use App\User;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function addUser(Request $request)
    {
        $data = array(
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'address' => $request->address,
            'hp' => $request->hp,
            //'npwp' => $request->npwp,
            //'role_id' => $request->role_id,
            'role_id' => 4,
            //'jabatan_id' => $request->jabatan_id
            'jabatan_id' => 6
        );

        DB::table('users')->insert($data);

        return Response::json($data);
    }

    public function getUser($id)
    {
        $user = User::find($id);
        return Response::json($user);
    }

    public function getUserByEmail($email)
    {
        $user = User::where('email', $email)->get();
        return Response::json($user);
    }

    public function editUser(Request $request)
    {
        $data = array(
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            //'password' => bcrypt($request->password),
            'address' => $request->address,
            'hp' => $request->hp,
            //'npwp' => $request->npwp,
            //'role_id' => $request->role_id,
            //'jabatan_id' => $request->jabatan_id
        );

        DB::table('users')->where('id','=',$request->id)->update($data);

        return Response::json($data);
    }

    public function deleteUser($id)
    {
        User::find($id)->delete();


        //return $this->response->noContent();
        return Response::json([
            'status' => 'ok']);
    }

    public function getAllUser()
    {
        $userData = User::all();

        return Response::json($userData);

    }

}
